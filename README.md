# Dodekalendar

Ein Dodekaeder ist ein Körper, dessen Oberfläche aus zwölf kongruenten regelmäßigen Fünfecken besteht. Ein Dodekaeder ist also ein platonischer Körper! Wenn wir auf jeden Seitenfläche einen Monatskalender drucken, so entsteht ein Dodekalendar.

## Dokument / Download

Die [aktuelle Version](https://mathezirkel.gitlab.io/content/dodekalendar/dodekalendar.pdf) des Dodekalendars wird automatisch aus diesem Repository kompiliert.
