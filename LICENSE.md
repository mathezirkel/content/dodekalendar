# Repository License Information

This repository contains a combination of code and content under various licenses.

## Source files and compiled files

The files in

* [src](./src/)
* [images](./images/)

and any compiled files derived from them are licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) or (at your opinion) any later version.

## Git, Build, and CI Files

Any other files are licensed under the [MIT License](https://opensource.org/licenses/MIT).

## Acknowledgments

In this project, various works and resources have been utilized, and we would like to extend our thanks to the following authors and communities:

* A code snippet from the documentation of the package [pgf](https://www.ctan.org/pkg/pgf), which is licensed under the under [LPPL (LaTeX Project Public License)](https://www.latex-project.org/lppl/), version 1.3c.
* A code snipped from an [answer of stackexchange](https://tex.stackexchange.com/questions/10186/weekday-captions-with-the-tikz-library-calendar), which is licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

---

You can find the corresponding licenses in the subfolder [licenses](./licenses/).
