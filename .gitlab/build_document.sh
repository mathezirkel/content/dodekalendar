#!/bin/sh

set -e
echo "Building document with latexmk..."
latexmk -cd -pdf ./src/dodekalendar.tex
mkdir public
mv src/dodekalendar.pdf public
